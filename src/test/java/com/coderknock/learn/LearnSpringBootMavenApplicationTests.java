package com.coderknock.learn;

import com.coderknock.learn.entity.Reader;
import com.coderknock.learn.repository.ReaderRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * 测试范例
 */
//SpringRunner 继承了 SpringJUnit4ClassRunner
@RunWith(SpringRunner.class)
@SpringBootTest
public class LearnSpringBootMavenApplicationTests {

	private static final Log logger = LogFactory.getLog(LearnSpringBootMavenApplicationTests.class);

	@Autowired
	ReaderRepository readerRepository;

	@Test
	public void contextLoads() {
		List<Reader> readerList = readerRepository.findAll();
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			logger.info(objectMapper.writeValueAsString(readerList));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

}
